﻿using Foxminded.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Foxminded.Services
{
    public class FileReader: IFileReader
    {
        public IEnumerable<string> ReadAllLines(string filePath) => File.ReadAllLines(filePath).Where(line => !string.IsNullOrWhiteSpace(line));
    }
}
