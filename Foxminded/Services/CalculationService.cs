﻿using Foxminded.Interfaces;
using Foxminded.Models;
using System.Collections.Generic;
using System.Linq;

namespace Foxminded.Services
{
    public class CalculationService: ICalculationService
    {
        /// <summary>
        /// Calculates index of file line with maximum sum of it's elements
        /// </summary>
        /// <param name="rows"></param>
        /// <returns>Line index unless input collection not empty otherwise -1.</returns>
        public int GetMaxCountLineIndex(IEnumerable<RowData> rows)
        {
            if (!rows.Any())
                return -1;

            var maxSumLineIndex = rows.GroupBy(x => x.Index, x => x.Items.Where(x => double.TryParse(x, out _)).Sum(x => double.Parse(x)))
                .OrderByDescending(x => x.First())
                .First()
                .Key;

            return maxSumLineIndex;
        }

        /// <summary>
        /// Calculates failed to parse line indexes
        /// </summary>
        /// <param name="rows"></param>
        /// <returns>Failed to parse line indexes</returns>
        public IEnumerable<int> GetParseFailedLineIndexes(IEnumerable<RowData> rows)
        {
            var indexes = rows.Where(x => x.Items.Any(d => !double.TryParse(d, out double result)))
                .Select(x => x.Index);

            return indexes;
        }
    }
}
