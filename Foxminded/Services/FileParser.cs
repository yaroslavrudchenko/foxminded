﻿using Foxminded.Interfaces;
using Foxminded.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Foxminded.Services
{
    public class FileParser: IFileParser
    {
        private readonly IFileReader _fileReader;

        public FileParser(IFileReader fileReader)
        {
            _fileReader = fileReader;
        }

        public IEnumerable<RowData> ParseAllLines(string filePath, string delimetr)
        {
            if (string.IsNullOrEmpty(filePath))
                throw new ArgumentNullException(nameof(filePath));

            if (string.IsNullOrEmpty(delimetr))
                throw new ArgumentNullException(nameof(delimetr));

            var lines = _fileReader.ReadAllLines(filePath)
                .Select((line, index) => new RowData { Index = ++index, Items = line.Split(delimetr) });

            return lines;
        }
    }
}