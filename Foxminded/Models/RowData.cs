﻿namespace Foxminded.Models
{
    public class RowData
    {
        public int Index { get; set; }

        public string[] Items { get; set; }
    }
}
