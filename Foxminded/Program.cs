﻿using Foxminded.Services;
using System;
using System.IO;
using System.Linq;

namespace Foxminded
{
    public class Program
    {
        static void Main(string[] args)
        {
            var delimetr = ",";
            var filePath = Path.Combine(Environment.CurrentDirectory, "test.txt");
            var calcService = new CalculationService();
            var fileParser = new FileParser(new FileReader());

            try
            {
                var rows = fileParser.ParseAllLines(filePath, delimetr);

                var maxLineIndex = calcService.GetMaxCountLineIndex(rows);
                var failedToParseIndexes = calcService.GetParseFailedLineIndexes(rows);

                Console.WriteLine($"-- Line Index with maximum sum of elements splitted by delimetr is: {maxLineIndex}");

                if (failedToParseIndexes.Any())
                    Console.WriteLine($"-- Line Indexes contains not parsed elements are: {string.Join(",", failedToParseIndexes)}");
                else
                    Console.WriteLine("-- All characters in file was valid and successfully parsed.");
            }
            catch(FileNotFoundException)
            {
                Console.WriteLine($"Further execution terminated. File you requested can't be found. FilePath: {filePath}");
            }

            Console.ReadKey();
        }
    }
}
