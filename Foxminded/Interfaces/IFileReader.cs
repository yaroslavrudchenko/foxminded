﻿using System.Collections.Generic;

namespace Foxminded.Interfaces
{
    public interface IFileReader
    {
        IEnumerable<string> ReadAllLines(string filePath);
    }
}
