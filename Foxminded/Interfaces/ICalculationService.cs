﻿using Foxminded.Models;
using System.Collections.Generic;

namespace Foxminded.Interfaces
{
    public interface ICalculationService
    {
        int GetMaxCountLineIndex(IEnumerable<RowData> rows);

        IEnumerable<int> GetParseFailedLineIndexes(IEnumerable<RowData> rows);
    }
}
