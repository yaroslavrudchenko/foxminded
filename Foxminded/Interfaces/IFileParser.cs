﻿using Foxminded.Models;
using System.Collections.Generic;

namespace Foxminded.Interfaces
{
    public interface IFileParser
    {
        IEnumerable<RowData> ParseAllLines(string filePath, string delimetr);
    }
}
