﻿using Foxminded.Interfaces;
using Foxminded.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;

namespace Foxminded.Tests.Services
{
    [TestFixture]
    public class FileParserTests
    {
        private Mock<IFileReader> _fileReaderMock;
        private FileParser _sut;

        private const string _defaultFileName = "dummyFile.txt";
        private const string _defaultDelimetr = ",";

        [SetUp]
        public void SetUp()
        {
            _fileReaderMock = new Mock<IFileReader>();
            _sut = new FileParser(_fileReaderMock.Object);
        }

        [Test]
        public void ParseAllLines_WhenFileIsEmpty_ReturnsEmptyList()
        {
            _fileReaderMock.Setup(method => method.ReadAllLines(It.IsAny<string>()))
                .Returns(Enumerable.Empty<string>());

            var result = _sut.ParseAllLines(_defaultFileName, _defaultDelimetr);

            Assert.IsFalse(result.Any());
        }

        [TestCase("1.23, 4, 5")]
        [TestCase("1.23, 4, 5,")]
        [TestCase("1,23, test")]
        [TestCase("1,23._!, 23.43, 6")]
        public void ParseAllLines_WhenFileContainsDataSeparatedByDelimetr_ReturnsFirstLineValues(string firstLine)
        {
            var fileLines = new[] { firstLine };
            _fileReaderMock.Setup(method => method.ReadAllLines(It.IsAny<string>()))
                .Returns(fileLines);

            var parsedLines = _sut.ParseAllLines(_defaultFileName, _defaultDelimetr);
            var firstLineData = parsedLines.First();

            Assert.IsTrue(parsedLines.Count() == fileLines.Length);
            CollectionAssert.AreEqual(firstLineData.Items, firstLine.Split(_defaultDelimetr));
        }

        [TestCase("1.23, 4, 5", "das, 43, 54, 5")]
        [TestCase("1.23, 4, 5,", "das, 43, 54, 5")]
        [TestCase("1,23, test", "das, 43, 54, 5")]
        [TestCase("1,23._!, 23.43, 6", "das, 43, 54, 5")]
        public void ParseAllLines_WhenFileContainsDataSeparatedByDelimetr_ReturnsLineValues(string fLine, string sLine)
        {
            var fileLines = new[] { fLine, sLine };
            var sourceParsedValues = fileLines.Select(x => x.Split(_defaultDelimetr))
                .SelectMany(x => x);
            _fileReaderMock.Setup(method => method.ReadAllLines(It.IsAny<string>()))
                .Returns(fileLines);

            var parsedLines = _sut.ParseAllLines(_defaultFileName, _defaultDelimetr);
            var targetParsedValues = parsedLines.SelectMany(x => x.Items);

            CollectionAssert.AreEqual(sourceParsedValues, targetParsedValues);
        }
    
        [Test]
        public void ParseAllLines_WhenFilePathIsNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => _sut.ParseAllLines(default, _defaultDelimetr));
        }

        [Test]
        public void ParseAllLines_WhenDelimetrIsNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => _sut.ParseAllLines(_defaultFileName, default));
        }

        [Test]
        public void ParseAllLines_WhenFilePathIsNull_CallForFileReader()
        {
            _fileReaderMock.Setup(method => method.ReadAllLines(_defaultFileName))
                .Returns(Enumerable.Empty<string>());

            _sut.ParseAllLines(_defaultFileName, _defaultDelimetr);

            _fileReaderMock.Verify(method => method.ReadAllLines(_defaultFileName), Times.Once);
        }
    }
}
