﻿using Foxminded.Models;
using Foxminded.Services;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Foxminded.Tests.Services
{
    [TestFixture]
    public class CalculationServiceTests
    {
        private CalculationService _sut;

        [SetUp]
        public void SetUp()
        {
            _sut = new CalculationService();
        }

        [Test]
        public void GetMaxCountLineIndex_WhenParsedFileLinesIsEmpty_ReturnsNegative1()
        {
            var rows = Enumerable.Empty<RowData>();

            var lineIndex = _sut.GetMaxCountLineIndex(rows);

            Assert.IsTrue(lineIndex == -1);
        }

        [Test]
        public void GetMaxCountLineIndex_WhenLineContainsNotANumberValues_ReturnsMaxSumLineIndex()
        {
            var sourceMaxSumLineIndex = 1;
            var rows = new List<RowData>
            {
                new RowData { Index = sourceMaxSumLineIndex, Items = new[]{ "1.5", "not parsed value", "1.5", "1,5" } },
                new RowData { Index = 2, Items = new[]{ "0.5", "1.5" } }
            };

            var targetMaxSumLineIndex = _sut.GetMaxCountLineIndex(rows);

            Assert.IsTrue(sourceMaxSumLineIndex == targetMaxSumLineIndex);
        }

        [Test]
        public void GetMaxCountLineIndex_WhenLineContainsParsedNumbers_ReturnsMaxSumLineIndex()
        {
            var sourceMaxSumLineIndex = 2;
            var rows = new List<RowData>
            {
                new RowData { Index = 1, Items = new[]{ "1.5", "4.5", "0.5" } },
                new RowData { Index = sourceMaxSumLineIndex, Items = new[]{ "6", "1" } }
            };

            var targetMaxSumLineIndex = _sut.GetMaxCountLineIndex(rows);

            Assert.IsTrue(sourceMaxSumLineIndex == targetMaxSumLineIndex);
        }

        [Test]
        public void GetMaxCountLineIndex_WhenLineContainsEqualsSum_ReturnsFirstLine()
        {
            var sourceMaxSumLineIndex = 1;
            var rows = new List<RowData>
            {
                new RowData { Index = sourceMaxSumLineIndex, Items = new[]{ "6", "1" } },
                new RowData { Index = 2, Items = new[]{ "6", "1" } }
            };

            var targetMaxSumLineIndex = _sut.GetMaxCountLineIndex(rows);

            Assert.IsTrue(sourceMaxSumLineIndex == targetMaxSumLineIndex);
        }

        [TestCase("1.23", "5.2", "5", "-1")]
        [TestCase("1", "5")]
        public void GetParseFailedLineIndexes_WhenLineContainsOnlyNumberValues_ReturnsEmptyIndexesList(params string[] lineValues)
        {
            var rows = new List<RowData>
            {
                new RowData { Index = 1, Items = lineValues }
            };

            var linesWithInvalidCharacters = _sut.GetParseFailedLineIndexes(rows);

            Assert.IsFalse(linesWithInvalidCharacters.Any());
        }

        [TestCase("!_23", "test", "...", "=4")]
        [TestCase("invalid")]
        public void GetParseFailedLineIndexes_WhenLineContainsInvalidValues_ReturnsNotEmptyList(params string[] lineValues)
        {
            var rows = new List<RowData>
            {
                new RowData { Index = 1, Items = lineValues }
            };

            var linesWithInvalidCharacters = _sut.GetParseFailedLineIndexes(rows);

            Assert.IsTrue(linesWithInvalidCharacters.Any());
        }
    }
}
